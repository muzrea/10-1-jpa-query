package com.twuc.webApp.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, String> {
    // TODO
    //
    // 如果需要请在此添加方法。
    //
    // <--start--
    public List<Product> getProductsByProductLine_TextDescriptionContains(String productDescription);
    public List<Product> getProductsByQuantityInStockBetween(short start,short end);
    public List<Product> getProductsByQuantityInStockBetweenOrderByProductCodeAsc(short start,short end);
    public Page<Product> getProductsByQuantityInStockBetween(short start, short end, Pageable pageable);
    // --end-->
}