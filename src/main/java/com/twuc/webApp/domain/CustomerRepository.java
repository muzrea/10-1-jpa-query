package com.twuc.webApp.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {
    // TODO
    //
    // 如果需要请在此添加方法。
    //
    // <--start--
    public List<Customer> getCustomersByCountryAndCity(String country, String city);
    // --end-->
}